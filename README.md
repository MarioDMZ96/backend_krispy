# Krispy Kreme Test Backend

## Run the project

1. Install all the project dependencies, run the following command:
```
npm install
```
2. Run the project with the following command:
```
npm run start
```

3. Modify the .env file with your local configuration

4. The database .sql file is located in /src/database/ 

5. Import the .sql file to your local database server

6. import the .sql procedures file

7. Users to login
```
admin@mail.com - secreta
empleado@mail.com - pa55word
```

![alt text](2.png)

![alt text](1.png)