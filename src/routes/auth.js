const { Router } = require('express');
const { signIn } = require('../controllers/authController');
const { user } = require('../services/dataValidator');

const router = Router();

router.post( '/signin', user.signIn() ,signIn);

module.exports = router;