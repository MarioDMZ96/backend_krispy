const { Router } = require('express');
const { readOrders, updateOrderStatus } = require('../controllers/ordersController');
const authValidate = require('../middlewares/authValidator');

const router = Router();

router.get( '/orders', readOrders );
router.put( '/orders/:id', authValidate ,updateOrderStatus);

module.exports = router;