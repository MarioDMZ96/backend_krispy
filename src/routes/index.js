const express = require('express');

const app = express();

// api routes
const authRoutes = require('./auth');
const orderRoutes = require('./orders');

app.get( '/', (req, res) => res.status(200).json({message: 'Welcome to krispy kreme!', error: false}) );
app.use( '/', authRoutes );
app.use( '/', orderRoutes );

module.exports = app;