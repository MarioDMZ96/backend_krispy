const jwt = require('jsonwebtoken');
const env = require('dotenv').config();

module.exports = jwtGenerate = (userData) => {
  return jwt.sign({
    id: userData.id,
    user: userData.name,
    email: userData.email,
    role: userData.user_type
    },
    process.env.JWT_SECRET,
    {expiresIn: '14 days'}
  );
}