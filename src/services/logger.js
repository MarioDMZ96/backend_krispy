const { createLogger, transports, format, info } = require("winston");
const env = require("dotenv").config();
const moment = require("moment");

module.exports = logger = (data) => {
  const currentDate = moment().format("DD-MM-YYYY");
  const currentTime = moment().format("HH:mm:ss");
  createLogger({
    format: format.combine(
      format.simple(),
      format.timestamp(),
      format.printf(
        (info) =>
          `[${currentTime}] -> ${data.type}: ${info.message}, [data] -> ${info.data}`
      )
    ),
    transports: [
      new transports.File({
        maxsize: 5120000,
        maxFiles: 10,
        filename: `${process.env.LOG_FILE}/action-log-${currentDate}.log`,
      }),
    ],
  }).info(data);
};
