const encrypt = require("../services/encrypt");
const logger = require("../services/logger");
const { validationResult } = require('express-validator');
const promiseQuery = require('../util/makeQueryPromise');
const jwtGenerate = require('../services/jwt');

const signIn = async (req, res) => {
  try {
    logger({
      type: "POST",
      message: "login user",
      data: JSON.stringify(req.body),
    });
    const errors = validationResult(req);
    if ( !errors.isEmpty() ){
      return res.status(400).json({error: true, message: 'Some errors was ocurred!', errors});
    }

    const { email , password } = req.body;
    const find_user_query = `SELECT * FROM users WHERE email = '${email}';`;
    const user_data = await promiseQuery(find_user_query);
    if ( user_data[0].email !== email || user_data[0].password !== encrypt(password) ){
      return res.status(200).json({
        error: true,
        message: `Email y/o contraseña son incorrectos`,
      });
    }
    return res.status(200).json({error: false, message: 'Bienvenido!', token: jwtGenerate(user_data[0]) });
  } catch (error) {
    return res.status(500).json({
      error: true,
      message: `Ocurrio un error: ${error.message}`,
    });
  }
}

module.exports = {
  signIn
};