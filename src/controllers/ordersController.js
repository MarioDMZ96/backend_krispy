const promiseQuery = require('../util/makeQueryPromise');

const readOrders = async (req,res) => {
  try {
    logger({
      type: "GET",
      message: "read orders",
      data: JSON.stringify(req.body),
    });
    let orders = await promiseQuery(`CALL read_orders();`);
    for ( let i = 0; i < orders[0].length; i++){
      orders[0][i].items = await promiseQuery(`CALL read_order_details(${orders[0][i].id});`);
    }
    if ( orders.length === 0){
      return res.status(400).json({error: true, message: 'No hay pedidos!'});
    }
    return res.status(200).json({error: false, message: 'Lista de ordenes!', orders});
  } catch (error) {
    return res.status(500).json({
      error: true,
      message: `Ocurrio un error: ${error.message}`,
    });
  }
}

const updateOrderStatus = async (req, res) => {
  try {
    logger({
      type: "PUT",
      message: "update order status",
      data: JSON.stringify(req.body),
    });
    const { status } = req.body;
    const order_id = req.params.id;
    if ( req.user_type === 2){
      return res.status(200).json({
        error: true,
        message: `No estas autorizado para realizar esta accion`,
      });
    }

    const updated = await promiseQuery(`CALL update_status(${order_id},${status});`);
    if (updated.affectedRows !== 1){
      return res.status(200).json({
        error: true,
        message: `No se pudo actualizar el status del pedido`,
      });
    }
    
    return res.status(200).json({
      error: false,
      message: `Status del pedido actualizado`,
    });
  } catch (error) {
    return res.status(500).json({
      error: true,
      message: `Ocurrio un error: ${error.message}`,
    });
  }
}

module.exports = {
  readOrders,
  updateOrderStatus
};