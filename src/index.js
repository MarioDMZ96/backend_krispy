// import packages
const express = require('express');
const cors = require('cors');
const env = require('dotenv').config();
const routes = require('./routes');

const Database = require('./database/Database');

const fs = require('fs');
const path = require('path');

const app = express();

// server middlewares
app.use( cors() );
app.use( express.json() );
app.use( express.urlencoded({ extended: false }) );

// routes
app.use( '/api', routes );

// start server
const APP_PORT = process.env.APP_PORT;
app.listen( APP_PORT, () => console.log('Server UP!') );
const db = new Database();
db.connection.connect(function(err) {
  if (err) {
    console.error('error connecting: ' + err.stack);
    return;
  }
  console.info('Database is connected!');
});