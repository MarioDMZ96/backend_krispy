const Database = require('../database/Database');
const db = new Database();

module.exports = promiseQuery = (query) => {
  return new Promise((resolve, reject) => {
    db.connection.query(query, (error, results) => {
      if(error) return reject(error); 
      return resolve(results); 
    });
  });
}
