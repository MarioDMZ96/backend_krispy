-- MySQL dump 10.13  Distrib 8.0.26, for macos11 (x86_64)
--
-- Host: localhost    Database: krispy_test
-- ------------------------------------------------------
-- Server version	8.0.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `customers` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (1,'Mario Dominguez','4433641277'),(2,'Alejandro Salgado','4431641277'),(3,'Diego Villanueva','4433641243'),(4,'Jorge Mejia','4433341377'),(5,'Bryan Mejia','4432451277'),(6,'Flor Aguilar','4431641677'),(7,'Ximena Dominguez','4431634577'),(8,'Alejandra Herrera','4439814677'),(9,'Ana Cortes','4431610567'),(10,'Alberto Moron','4431274677'),(11,'Pedro Aviles','4431323677'),(12,'Erik Garcia','4412441677');
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `delivery_men`
--

DROP TABLE IF EXISTS `delivery_men`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `delivery_men` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `deliveries` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `delivery_men`
--

LOCK TABLES `delivery_men` WRITE;
/*!40000 ALTER TABLE `delivery_men` DISABLE KEYS */;
INSERT INTO `delivery_men` VALUES (1,'Mario Dominguez','4433641277',49),(2,'Alejandro Salgado','4431641277',200),(3,'Diego Villanueva','4433641243',143),(4,'Jorge Mejia','4433341377',3);
/*!40000 ALTER TABLE `delivery_men` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_details`
--

DROP TABLE IF EXISTS `order_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_details` (
  `order_id` int DEFAULT NULL,
  `item` varchar(20) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `comment` text,
  `extra` varchar(20) DEFAULT NULL,
  KEY `fk_order_id` (`order_id`),
  CONSTRAINT `fk_order_id` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_details`
--

LOCK TABLES `order_details` WRITE;
/*!40000 ALTER TABLE `order_details` DISABLE KEYS */;
INSERT INTO `order_details` VALUES (1,'dona de chocolate',25.6,NULL,NULL),(1,'dona de m&m',35,NULL,NULL),(1,'frappe',75,NULL,'chispas'),(2,'dona de chocolate',25.6,NULL,NULL),(2,'dona de m&m',30.4,NULL,NULL),(3,'dona de chocolate',25.6,NULL,NULL),(3,'dona de m&m',30.4,NULL,NULL),(3,'dona de azucar',25.5,NULL,NULL),(3,'dona de azucar',25.5,NULL,NULL),(4,'docena donas surtida',120,NULL,NULL),(5,'frappe mocha',40,'doble vainilla',NULL),(5,'frappe vainilla',40.4,'sin popote',NULL),(6,'frappe mocha',40,'doble vainilla',NULL),(6,'frappe vainilla',40.4,'sin popote',NULL),(7,'dona glaseada',20,NULL,NULL),(7,'dona de azucar',20,NULL,NULL),(8,'docena donas',260,NULL,NULL),(8,'frappe mocha',70,NULL,NULL),(9,'docena donas',120,NULL,NULL),(10,'pay fresa',20,NULL,NULL),(10,'pay queso',20,NULL,NULL),(11,'pay fresa',20,NULL,NULL),(11,'docena donas',20,NULL,NULL),(12,'frappe oreo',85,'sin popote',NULL),(13,'docena donas',120,NULL,NULL),(13,'frappe fresa',80,NULL,NULL),(13,'docena donas',120,NULL,NULL),(14,'frappe fresa',80,NULL,NULL),(15,'docena donas',120,NULL,NULL),(15,'bolitas dona',110,NULL,NULL),(16,'docena donas',120,NULL,NULL),(16,'docena donas',120,NULL,NULL),(16,'docena donas',120,NULL,NULL),(16,'frappe fresa',50,NULL,'chispas'),(17,'frappe fresa',80,NULL,NULL),(18,'frappe fresa',80,NULL,NULL);
/*!40000 ALTER TABLE `order_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `orders` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_number` int DEFAULT NULL,
  `status` int DEFAULT NULL,
  `payment_method` varchar(20) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `sub_total` double DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `promo_code` varchar(8) DEFAULT NULL,
  `customer` int DEFAULT NULL,
  `delivery_man` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_customer` (`customer`),
  KEY `fk_delivery_man` (`delivery_man`),
  CONSTRAINT `fk_customer` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`),
  CONSTRAINT `fk_delivery_man` FOREIGN KEY (`delivery_man`) REFERENCES `delivery_men` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,123,1,'efectivo',135.5,120,0,NULL,1,2),(2,124,1,'efectivo',55,60,10,'SEPT10',2,1),(3,125,0,'tarjeta',105.5,100,0,NULL,5,4),(4,126,3,'efectivo',135.5,120,0,NULL,1,2),(5,127,0,'tarjeta',75.5,80,5,'SEPT5',10,3),(6,128,3,'tarjeta',85.5,80,0,NULL,11,1),(7,129,1,'tarjeta',25.5,40,15,'SEPT15',6,1),(8,130,0,'efectivo',345.5,330,0,NULL,4,2),(9,131,3,'efectivo',95.5,120,25.5,'MX21',9,4),(10,132,0,'tarjeta',45.5,40,0,NULL,10,3),(11,133,1,'efectivo',90,120,30,'SEPT30',7,1),(12,134,0,'tarjeta',100,85,0,NULL,2,3),(13,135,2,'tarjeta',90,200,110,'CENA100',4,1),(14,136,0,'efectivo',75.5,80,5,'SEPT5',10,3),(15,137,0,'tarjeta',250,230,0,NULL,8,2),(16,138,0,'efectivo',390.5,420,30,'WIN30',5,4),(17,139,0,'tarjeta',75.5,80,5,'SEPT5',10,3),(18,140,0,'tarjeta',98.5,80,0,NULL,3,2);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_types` (
  `id` int NOT NULL,
  `name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_types`
--

LOCK TABLES `user_types` WRITE;
/*!40000 ALTER TABLE `user_types` DISABLE KEYS */;
INSERT INTO `user_types` VALUES (1,'admin'),(2,'employee');
/*!40000 ALTER TABLE `user_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `password` text,
  `user_type` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_type` (`user_type`),
  CONSTRAINT `fk_user_type` FOREIGN KEY (`user_type`) REFERENCES `user_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Roberto Martinez','admin@mail.com','4f356e71899d4e6ac647d2ed22a81dd2',1),(2,'Cesar Huerta','empleado@mail.com','a17a41337551d6542fd005e18b43afd4',2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-11 14:51:47
