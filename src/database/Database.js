const mysql = require('mysql');
const dbConfig = require('./databaseConfig');

module.exports = class Database {

  static database;

  constructor (){
    this.connection = mysql.createConnection(dbConfig);

    if ( !!Database.database ){
      return Database.database;
    }

    Database.database = this;
  }

}
